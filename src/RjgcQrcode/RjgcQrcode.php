<?php

/**
 * Created by PhpStorm.
 * User: My
 * Date: 2018/4/20
 * Time: 16:44
 */
namespace RjgcQrcode;

include dirname(__FILE__).'/Lib/phpqrcode/qrlib.php';
class RjgcQrcode
{



    public function encodeMask(QRinput $input, $mask)
    {
        return (new \QRcode())->encodeMask( $input, $mask);
    }


    public function encodeInput(QRinput $input)
    {
        return (new \QRcode())->encodeInput($input);
    }

    public function encodeString8bit($string, $version, $level)
    {
        return (new \QRcode())->encodeString8bit($string, $version, $level);
    }


    public function encodeString($string, $version, $level, $hint, $casesensitive)
    {

        return (new \QRcode())->encodeString($string, $version, $level, $hint, $casesensitive);
    }

    /**
     * @param $text 内容
     * @param bool $outfile 生成二维码存放的位置,若为FALSE为直接输出
     * @param int $level 级别
     * @param int $size 尺寸
     * @param int $margin 边距
     * @param bool $saveandprint 保存并打印
     */
    public static function png($text, $outfile = false, $level = QR_ECLEVEL_L, $size = 3, $margin = 4, $saveandprint=false)
    {
        return \QRcode::png($text, $outfile , $level, $size, $margin, $saveandprint);
    }


    public static function text($text, $outfile = false, $level = QR_ECLEVEL_L, $size = 3, $margin = 4)
    {
        return \QRcode::text($text, $outfile, $level, $size , $margin );

    }


    public static function raw($text, $outfile = false, $level = QR_ECLEVEL_L, $size = 3, $margin = 4)
    {
        return \QRcode::raw($text, $outfile, $level , $size , $margin );
    }
}